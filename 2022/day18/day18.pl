#!/usr/bin/perl
use v5.18;
use warnings;
use List::Util qw(max min sum0);

my %lookup;
my $part1 = 0;
my @box = ([~0, 0], [~0, 0], [~0, 0]);

sub neighbor {
	my ($x, $y, $z) = @_;
	return sum0 map { exists $lookup{"@$_"} }
		[$x - 1, $y, $z],
		[$x + 1, $y, $z],
		[$x, $y - 1, $z],
		[$x, $y + 1, $z],
		[$x, $y, $z - 1],
		[$x, $y, $z + 1];
}

while (<>) {
	my @c = map { int } /\d+/g;
	for (0 .. 2) {
		$box[$_]->[0] = min $box[$_]->[0], $c[$_];
		$box[$_]->[1] = max $box[$_]->[1], $c[$_];
	}
	$lookup{"@c"} = undef;
	$part1 += 6 - 2 * neighbor(@c);
}

say "part 1: $part1";
